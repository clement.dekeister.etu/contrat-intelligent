// SPDX-License-Identifier: MIT
pragma solidity ^0.8.22;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Voting is Ownable(msg.sender) {

    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
        uint weight; 
    }

    struct Proposal {
        string description;
        uint voteCount;
    }

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    WorkflowStatus public workflowStatus;
    
    mapping(address => Voter) public voters;
    uint public proposalEndTime;
    uint public votingEndTime;
    
    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event Voted (address voter, uint proposalId);


    modifier onlyBefore(WorkflowStatus _status) {
        require(workflowStatus < _status, "This action is not allowed at this stage.");
        _;
    }
    
    modifier onlyAtStatus(WorkflowStatus _status) {
        require(workflowStatus == _status, "This action is not allowed at this stage.");
        _;
    }

    modifier onlyRegisteredVoter() {
        require(voters[msg.sender].isRegistered, "Only registered voters can perform this action.");
        _;
    }

    modifier hasNotVoted() {
        require(!voters[msg.sender].hasVoted, "Voter has already voted.");
        _;
    }

    // Register voters
    function addVoter(address _voterAddress, uint _weight) external onlyOwner onlyBefore(WorkflowStatus.ProposalsRegistrationStarted) {
        require(!voters[_voterAddress].isRegistered, "The voter is already registered.");

        voters[_voterAddress].isRegistered = true;
        voters[_voterAddress].weight = _weight;
        emit VoterRegistered(_voterAddress);
    }

    // debut session ajout proposition
    function startProposalsRegistration() external onlyOwner onlyAtStatus(WorkflowStatus.RegisteringVoters) {
        WorkflowStatus previousStatus = workflowStatus;
        workflowStatus = WorkflowStatus.ProposalsRegistrationStarted;
        proposalEndTime = block.timestamp + 1 weeks;
        emit WorkflowStatusChange(previousStatus, workflowStatus);
    }

    //  fin session ajout propositions
    function checkProposalSessionEnd() external onlyOwner onlyAtStatus(WorkflowStatus.ProposalsRegistrationStarted) {
        if (block.timestamp > proposalEndTime) {
            WorkflowStatus previousStatus = workflowStatus;
            workflowStatus = WorkflowStatus.ProposalsRegistrationEnded;

            emit WorkflowStatusChange(previousStatus, workflowStatus);
        }
    }

    // début du vote
    function startVotingSession() external onlyOwner onlyAtStatus(WorkflowStatus.ProposalsRegistrationEnded) {
        WorkflowStatus previousStatus = workflowStatus;
        workflowStatus = WorkflowStatus.VotingSessionStarted;
        votingEndTime = block.timestamp + 1 weeks;

        emit WorkflowStatusChange(previousStatus, workflowStatus);
    }

    // fin du vote
    function checkVotingSessionEnd() external onlyOwner onlyAtStatus(WorkflowStatus.VotingSessionStarted) {
        if (block.timestamp > votingEndTime) {
            WorkflowStatus previousStatus = workflowStatus;
            workflowStatus = WorkflowStatus.VotingSessionEnded;

            emit WorkflowStatusChange(previousStatus, workflowStatus);
        }
    }

    function vote(uint proposalId) external onlyRegisteredVoter hasNotVoted onlyAtStatus(WorkflowStatus.VotingSessionStarted) {
        require(proposalId < proposals.length, "Invalid proposal ID.");

        voters[msg.sender].hasVoted = true;
        voters[msg.sender].votedProposalId = proposalId;

        proposals[proposalId].voteCount += voters[msg.sender].weight; // Prenant en compte le poids de l'électeur
        emit Voted(msg.sender, proposalId);
    }


    Proposal[] public proposals;

    event ProposalRegistered(uint proposalId);


    modifier onlyDuringProposalRegistration() {
        require(workflowStatus == WorkflowStatus.ProposalsRegistrationStarted, "Proposal registration is not open.");
        _;
    }

    function registerProposal(string calldata proposalDescription) 
        external 
        onlyRegisteredVoter 
        onlyDuringProposalRegistration
    {
        proposals.push(Proposal({
            description: proposalDescription,
            voteCount: 0
        }));

        emit ProposalRegistered(proposals.length - 1);
    }

    //getter pour une proposition spécifique
    function getProposalVotes(uint proposalId) public view returns (string memory description, uint voteCount) {
        require(proposalId < proposals.length, "Proposal ID out of bounds");

        Proposal memory proposal = proposals[proposalId];
    return (proposal.description, proposal.voteCount);
    }

    // getter pour toutes les propositions
    function getAllProposals() public view returns (Proposal[] memory) {
    return proposals;
    }

    //savoir quelle proposition a le plus de votes
        function getWinningProposal() public view returns (string memory winningProposalDescription, uint voteCount) {
        uint highestVoteCount = 0;
        uint winningProposalIndex = 0;

        for (uint i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > highestVoteCount) {
                highestVoteCount = proposals[i].voteCount;
                winningProposalIndex = i;
            }
        }

        return (proposals[winningProposalIndex].description, highestVoteCount);
    }

}


