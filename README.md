# Système de Vote Ethereum

## Fonctionnalités ajoutées
### Vote pondéré
Au lieu d'avoir un vote par personne, j'ai intégré la notion de "poids" pour les votes. Cela signifie que certains électeurs peuvent avoir un poids plus lourd dans le vote, en fonction de la valeur attribuée lors de leur enregistrement.

### Arrêt automatique des votes
Au lieu de nécessiter une intervention manuelle pour clore la période de vote, j'ai implémenté une fonctionnalité qui ferme automatiquement la session de vote au bout de 7 jours.

## Partie 3
Pendant la troisième partie, j'ai rencontré un problème lors du déploiement du contrat sur la blockchain. L'erreur rencontrée était `UNPREDICTABLE_GAS_LIMIT`, qui indique qu'il n'était pas possible de prévoir la quantité de gaz nécessaire pour exécuter la transaction. Je suis actuellement en train de rechercher des solutions pour résoudre ce problème.
